import unittest
import eval,tokenizer,sys
from StringIO import StringIO

class my_test(unittest.TestCase):
    
    def test_add(self):
        '''
        test addition with two arguments and multiple arguments
        '''
        self.assertEqual(eval.eval(tokenizer.parse("(+ 1 2)")), 3)
        self.assertEqual(eval.eval(tokenizer.parse("(+ 1 2 3 4)")), 10)  
        
    def test_multiply(self):
        '''
        test multiplication with two arguments and multiple arguments
        '''
        self.assertEqual(eval.eval(tokenizer.parse("(* 1 2 3 4)")), 24)
        self.assertEqual(eval.eval(tokenizer.parse("(* 1 2)")), 2)

    def test_sub(self):
        '''
        test subtraction with two arguments and multiple arguments
        '''
        self.assertEqual(eval.eval(tokenizer.parse("(- 30 20)")), 10)
        self.assertEqual(eval.eval(tokenizer.parse("(- 30 20 10)")), 0)

    def test_div(self):
        '''
        test division with two arguments and multiple arguments
        '''
        self.assertEqual(eval.eval(tokenizer.parse("(/ 10 2)")), 5)
        self.assertEqual(eval.eval(tokenizer.parse("(/ 10 2 5)")), 1)

    def invalid_expression(self,s):
        '''
        s-->list
        Helper function to test for invalid expressions
        '''
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            with self.assertRaises(SystemExit) as cm:
                eval.helper(s)
            output = out.getvalue().strip()
            assert output == 'Invalid Program'
        finally:
            sys.stdout = saved_stdout

    def test_invalid_expressions(self):
        '''
        Function to test invalid expressions
        '''
        a = [["()"], ["(+ 1)"], ["(+ 1)"], ["(/ 10 0)"], ["A"], ["(+ (* 1 2) + (* 3 4))"], ["(+ 1) 2)"],["(bind bind 10)"]]
        for i in a:
            self.invalid_expression(i)

    def test_bind(self):
        '''
        Test bind special symbol
        '''
        s = ["(bind length 10)"]
        self.assertEqual(eval.helper(s),10)

    def test_multiple_statements(self):
        '''
        Test multiple expressions
        '''
        s = ["(bind a 10)", "(bind b a)", "(bind a 11)", "(+ a b)"]
        a = ["(bind length (+ 1 2))", "(+ 1 2 3 4)", "(bind breadth length)", "(bind length 10)", "(bind area (* length breadth))"]
        b = ["(+ 1 (* 2 3) (* 4 2))"]
        self.assertEqual(eval.helper(s),21)
        self.assertEqual(eval.helper(a),30)
        self.assertEqual(eval.helper(b),15)

unittest.main()

