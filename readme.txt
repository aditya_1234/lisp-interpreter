The program (eval.py) takes the path to the expressions file as input and outputs the result of evaluating the last expression or outputs Invalid Program if there is an error in the program. 

Requirements:
	1. Need Python 2.7.9 to run the program.

There are three modules tokenizer.py,eval.py and test.py. 
Tokenizer tokenizes the program.
Eval evaluates the program. 
Test is for unit testing. It tests valid expressions and invalid expressions (test.py can be referred for better understanding). 

Assumptions:

	1. The text file contains expressions with each expression in a single line check input.txt for sample expressions.
	2. Variable name cannot be the special symbol "bind". 
	3. The expression (- 2 3) is evaluated as 2-3=-1
	4. Program prints Invalid program for (/ 4 0) i.e. 4/0 gives an invalid program
	
Working:

	Major steps involved are tokenize and evaluating the environment.
	
	Tokenize:
		In tokenize we create a list of tokens from the expression
		
	Evaluating:
		In evaluating, we evaluate each list of expression recursively. We use a global dictionary to store the values of the variables. 
		
Running example:
	$python eval.py
	Enter the path to the file C:\Users\pain\Desktop\test\yink-psil-task\input.txt
	Invalid Program